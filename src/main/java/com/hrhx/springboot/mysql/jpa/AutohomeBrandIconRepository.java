package com.hrhx.springboot.mysql.jpa;

import com.hrhx.springboot.domain.AutohomeBrandIcon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutohomeBrandIconRepository extends JpaRepository<AutohomeBrandIcon,Long> {
}
